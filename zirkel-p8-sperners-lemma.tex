% Hier den Pfad zu zirkel.cls angeben
\documentclass{../tex-bilder/zirkel}

\graphicspath{{../tex-bilder/}}

\geometry{top=3cm, bottom=2.5cm}


\usepackage{mdframed}
\usepackage{array}      % for spacing in tabular

\usepackage{wrapfig,lipsum}     % image to the side with text wrapped around
% \usepackage{caption}            % custom caption starters

\renewcommand{\schuljahr}{2022/23}
\renewcommand{\klasse}{7 \& 8}
\renewcommand{\titel}{Kunterbunte Dreiecksgitter}
\renewcommand{\untertitel}{Präsenzzirkel vom 31. März 2023}
\renewcommand{\name}{Alexander Mai}

% Vordefinierte Umgebungen
%     * aufgabe
%     * hinweis
%     * loesung
%     * satz
%     * beispiel

\begin{document}
    \makeheader

    \vspace*{-4em}
    \section{Dreieckssgitter}

    Wir beginnen mit einem großen Dreieck, in das wir beliebig viele weitere Strecken einsetzen, sodass alle leeren Flächenstücke Dreiecke sind, auf deren Seiten keine Gitterpunkte liegen. Das bezeichnen wir als \emph{Dreiecksgitter}. Nicht erlaubt sind also zum Beispiel Vierecke, die wie Dreiecke aussehen. Denn eine der Ecken hat dann einen Winkel von 180°, beziehungsweise auf einer Dreiecksseite liegt ein Gitterpunkt.

    \begin{figure}[h!]
        \label{fig:dreiecksgitterexamples}
        \centering
        \includegraphics[scale=0.35]{sperner/grids-examples-exercise.pdf}
        \vspace*{-0.5em}
        \caption{Nicht alle Abbildungen zeigen ein echtes Dreiecksgitter.}
    \end{figure}

    \vspace*{-1.5em}

    \begin{aufgabe}[Dreiecksgitter]
        In Abbildung~\ref{fig:dreiecksgitterexamples} siehst du fünf Gitter.
        \vspace*{-0.5em}
        
        \begin{enumerate}[a)]
            \item Welche davon sind echte Dreiecksgitter? % 1 und 2
            \vspace*{-0.2em}
            \item Welche davon sind keine echten Dreiecksgitter? Finde jeweils einen Grund, warum nicht. % Letzte 3 alle nicht. 3) außen kein Dreieck, 4) an zwei Stellen ist ein Dreieck in echt ein Viereck mit einem 180°-Winkel, 5) unten rechts ist ein echtes Viereck
            \vspace*{-0.2em}
            \item Wie könntest du die falschen Dreiecksgitter durch Hinzufügen weiterer Strecken korrigieren zu echten Dreiecksgittern?
        \end{enumerate}
    \end{aufgabe}
    \vspace*{-1em}

    \section{Mindestens ein kunterbuntes Dreieck}

    \begin{wrapfigure}{r}{5.5cm}
        \label{fig:examplesperner}
        \vspace*{-3em}
        \hspace*{1em}\includegraphics[scale=0.3]{sperner/sperner-example.pdf}
        \vspace*{-0.5em}
        \caption{Bsp Dreiecksgitter}
        % \caption*{Abb 2: gefärbtes Dreiecksgitter}
        \vspace*{-3em}
    \end{wrapfigure}

    Wir wählen drei Farben und färben damit jeden Gitterpunkt ein. Dafür beachten wir ein paar Regeln:
    \begin{itemize}
        \item die drei Ecken des äußersten, größten Dreiecks färben wir jeweils verschieden, also mit allen drei Farben
        \item auf jeder Seite des äußersten Dreiecks färben wir alle Punkte nur mit den zwei Farben der begrenzenden Ecken ein, also nicht mit der gegenüberliegenden Farbe
        \item zuletzt färben wir alle inneren Punkte beliebig ein
    \end{itemize}
    
    Wenn die Ecken eines kleinen Dreiecks mit allen verschiedenen Farben eingefärbt sind, nennen wir es ein \emph{kunterbuntes Dreieck}. Zähle nach, wie viele kunterbunte Dreiecke in Abbildung~\ref{fig:examplesperner} sind. \\
    \vspace*{-1.5em}
    
    \enlargethispage{5\baselineskip}

    \begin{satz}[Sperners Lemma, zweidimensionaler Fall]\label{satz:sperner2dim}
        Färben wir ein Dreiecksgitter nach diesen Regeln ein, gibt es immer mindestens ein kleines kunterbuntes Dreieck.
    \end{satz}

    \newpage



    \section{Der eindimensionale Fall}

    Wir betrachten in diesem Kapitel eine einzelne Seite eines Dreiecksgitters. Es stehen uns also nur zwei Farben zur Verfügung. Wir interessieren uns vor allem für \emph{bunte Paare} -- also zwei direkt nebeneinander liegende Punkte, die verschiedene Farben haben.

    \begin{figure}[h!]
        \label{fig:1dimexamples}
        \centering
        \includegraphics[scale=0.32]{sperner/1-dim-examples.pdf}
        \caption{Liniengitter, drei davon eingefärbt}
    \end{figure}


    \begin{aufgabe}[Anzahl bunter Paare]
        In Abbildung~\ref{fig:1dimexamples} siehst du drei Beispiele für eingefärbte Liniengitter.
        
        \begin{enumerate}[a)]
            \item Zähle die Anzahl bunter Paare für jedes Beispiel. Was fällt dir auf? % 1, 3, 5 -> ungerade Zahlen
            \vspace*{-0.2em}
            \item Beweise deine Vermutung. % per Widerspruchsbeweis: angenommen gerade viele bunte Paare. Ganz links sei Farbe A, ganz rechts Farbe B. Bei gerade vielen bunten Paaren, würde man also von ganz links nach ganz rechts gerade oft die Farbe wechseln. Dann wäre aber ganz rechts auch Farbe A.
            \vspace*{-0.2em}
            \item \textbf{Bonus:} Wie viele bunte Paare sind maximal möglich, bei einer Linie mit $n$-vielen Punkten? % die nächst kleinere ungerade Zahl ist maximal möglich, also n-1 falls n gerade, n-2 falls n ungerade
        \end{enumerate}
    \end{aufgabe}


    \section{Der zweidimensionale Fall}

    Wir betrachten nun ein einzelnes kleines Dreieck mit gefärbten Ecken und bestimmen zwei unserer drei Farben als unsere \emph{Pfadfarben}. Im Folgenden seien \textcolor{red}{\textbf{rot}} und \textcolor{blue}{\textbf{blau}} unsere Pfadfarben. Interpretiere jetzt jede Dreiecksseite als einen Durchgang, wenn diese Seite zwischen unseren beiden Pfadfarben liegt -- in unserem Fall also eine Seite mit einer \textcolor{red}{\textbf{roten}} und einer \textcolor{blue}{\textbf{blauen}} Ecke. Alle anderen Seiten interpretieren wir als eine Wand, die nicht überwunden werden kann.

    \begin{aufgabe}[Durchgänge und Mauern]
        \vspace*{-1em}   
        \begin{enumerate}[a)]
            \item Finde Beispiele für kleine Dreiecke mit $0, 1, 2, 3, 4, \dots$ Durchgängen. \emph{Achtung:} nicht für jede Anzahl an Durchgängen gibt es passende Beispiele. Begründe jeweils. % 0: eine der Pfadfarben fehlt. 1: kunterbuntes Dreieck (zwei mögliche Beispiele, inklusive gespiegelte Version, Drehungen nicht extra gezählt). 2: eine Pfadfarbe einmal, andere Pfadfarbe zweimal (zwei mögliche Beispiele). 4 und größer: nicht möglich, da ein Dreieck nur 3 Seiten hat und damit höchstens 3 Durchgänge. Aber 3 auch nicht möglich: Entweder alle möglichen Färbungen aufmalen, oder Widerspruchsbeweis: Angenommen, ein Dreieck hätte 3 Durchgänge. Seien a, b, c jeweils die drei Farben. Bei einem Durchgang sind die Farben der beiden Ecken verschieden, ungeachtet der eigentlichen Pfadfarben. D.h. es gilt: a \neq b, b \neq c, c \neq a. Aber dann haben wir bereits ein kunterbuntes Dreieck, ohne überhaupt über Pfadfarben nachzudenken, und dieses hat nur einen Durchgang.
            \vspace*{-0.2em}
            \item Fällt dir für eine bestimmte Anzahl an Durchgängen etwas auf? % kunterbuntes Dreieck bei einem Durchgang.
            \vspace*{-0.2em}
            \item Was gilt für die Anzahl an Durchgängen beim eindimensionalen Fall und welche Rolle spielen dafür jeweils die Randfarben? % wenn die Pfadfarben die Linie begrenzen, dann genau so viele Durchgänge, wie bunte Paare. Ansonsten keine Durchgänge, da eine Pfadfarbe fehlt.
        \end{enumerate}
    \end{aufgabe}

    Wir wollen nun beispielhaft für ein paar bunte Dreiecksgitter alle entstehenden Pfade einzeichnen, indem wir alle anliegenden Dreiecke verbinden, zwischen denen es einen Durchgang gibt. Nutze dafür das Beispiel von Seite~\pageref{fig:examplesperner} oder die Spielflächen vom anderen Zettel.

    \begin{aufgabe}[Auf der Suche nach dem kunterbunten Dreieck]
        Was fällt dir an den Pfaden auf? Schneiden sie sich? Spalten sie sich auf? Wo befinden sich Start und Ende eines Pfads? Versuche, deine Vermutungen zu begründen. \textbf{Finale:} Beweise Satz~\ref{satz:sperner2dim}. % Schneiden sich nicht, weil höchstens 2 Durchgänge. Spalten sich deswegen auch nicht auf (also keine Weggabelung). Start und Ende eines Pfades sind entweder in einem kunterbunten Dreieck, oder an dem Gitterrand, der die zwei Durchgangsfarben hat -> im Beispielbild die untere Dreiecksgitterseite.
        % Finale: Jeder Pfad hat 2 Enden. Jedes kunterbunte Dreieck ist ein Ende eines Pfades. Wir wissen, dass es auf der Unterseite des Dreiecksgitters ungerade viele Durchgänge gibt. Angenommen, alle Pfade würden am unteren Dreiecksgitter enden (und starten), also es gibt kein kunterbuntes Dreieck. Das ist nicht möglich, da es an der unteren Dreiecksgitterseite ungerade viele Durchgänge gibt. Da jeder Pfad zwei Enden hat und Pfade sich weder schneiden, noch aufgabeln, und es keine Durchgänge an den anderen Gitterseiten gibt, muss ein Pfadende im Inneren des Dreiecksgitters liegen, also in einem kunterbunten Dreieck. Bonusfact: Jeder Pfad, der im Inneren des Gitter beginnt und endet, hat genau 2 kunterbunte Dreiecke an den Enden. Die Existenz solcher innerer Pfade ist nicht gegeben, aber wenn es solche gibt, dann tragen sie jeweils 2 kunterbunte Dreiecke bei. Außerdem kann es mehrere Pfade geben, die von unten kommen, aber nicht wieder unten rauskommen -- von denen gibt es aber immer ungerade viele. Daraus folgern wir, dass es immer ungerade viele kunterbunte Dreiecke gibt.
    \end{aufgabe}

\end{document}